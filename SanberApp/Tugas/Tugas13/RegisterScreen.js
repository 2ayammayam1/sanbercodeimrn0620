import React, {Component} from 'react';
import { StyleSheet, Image, Text, View, TextInput, TouchableOpacity } from 'react-native';
import Header from './assets/header'


export default class RegisterScreen extends Component {
    render() {
    return (
      <View style={styles.container}>
        <Header />
        <Text style={styles.titleText}>Register</Text>
        <View style={{padding: 15, marginHorizontal: 50}}>
          <Text>Username</Text>
          <TextInput style={styles.input}/>
        </View>
        <View style={{padding: 15, marginHorizontal: 50}}>
          <Text>Email</Text>
          <TextInput style={styles.input}/>
        </View>
        <View style={{padding: 15, marginHorizontal: 50}}>
          <Text>Password</Text>
          <TextInput style={styles.input}/>
        </View>
        <View style={{padding: 15, marginHorizontal: 50}}>
          <Text>Ulangi Password</Text>
          <TextInput style={styles.input}/>
        </View>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity style={[styles.button, {backgroundColor: '#003366'}]}>
            <Text style={styles.buttonText}>Daftar</Text>
          </TouchableOpacity>
          <Text style={{color: '#3EC6FF', fontSize: 24}}>Atau</Text>
          <TouchableOpacity style={[styles.button, {backgroundColor: '#3EC6FF'}]}>
            <Text style={styles.buttonText}>Masuk?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
    }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    flex: 1
  },
  titleText: {
    textAlign: 'center',
    color: '#003366',
    fontSize: 24,
    margin: 20,
    fontFamily: 'Roboto'
  },
  input: {
    borderWidth: 1,
    borderColor: '#003366',
    height: 48
  },
  inputTitle: {
    borderColor: '#003366'
  },
  button: {
    width: 140,
    height: 40,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10
  }, 
  buttonText: {
    color: '#FFFFFF',
    fontSize: 24
  }
  
});
