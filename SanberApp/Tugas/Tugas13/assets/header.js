import React, {Component} from 'react';
import { StyleSheet, Image, Text, View, TextInput } from 'react-native';


export default class Header extends Component {
    render() {
        return (
        <View style={styles.container}>
            <Image style={styles.image} source={require('../images/sanberlogo.png')} />
            <Text style={styles.logoText}>PORTOFOLIO</Text>
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 60,
    alignItems: 'center',
    backgroundColor: '#fff',
    marginHorizontal:100
  }, 
  image: {
    width: 300,
    height: 48
  },
  logoText: {
    color: '#3EC6FF',
    fontSize: 24,
    textAlign: 'left'
  }
});
