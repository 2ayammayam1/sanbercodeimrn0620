import React, { Component } from 'react'
import { View, Text, StyleSheet, Button } from "react-native";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
//import { SignIn, CreateAccount, Profile, Home, Search, Details, Search2} from "./Screen";

import LoginScreen from "./Screens/LoginScreen";
import AboutScreen from "./Screens/AboutScreen";
import SkillScreen from "./Screens/SkillScreen";
import AddScreen from "./Screens/AddScreen";
import ProjectScreen from "./Screens/ProjectScreen";




//const AuthStack = createStackNavigator();
const Tabs  = createBottomTabNavigator();


const TabsScreen = () => (
  <Tabs.Navigator>
      <Tabs.Screen name="Skills" component={SkillScreen} />
      <Tabs.Screen name="Projects" component={ProjectScreen} />
      <Tabs.Screen name="Add" component={AddScreen} />
    </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();


export default () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator>
         <Drawer.Screen name="Login" component={LoginScreen} />
         <Drawer.Screen name="Tabs" component={TabsScreen} />
         <Drawer.Screen name="About" component={AboutScreen} />
     </Drawer.Navigator>
    </NavigationContainer>
  )
}

    // <NavigationContainer>
    // <Drawer.Navigator>
    //   <Drawer.Screen name="Login" component={LoginScreen} />
    //   <Drawer.Screen name="Tabs" component={TabsScreen} />
    //   <Drawer.Screen name="About" component={AboutScreen} />
    // </Drawer.Navigator>
  //   {/* <AuthStack.Navigator>
  //     <AuthStack.Screen 
  //       name="SignIn" 
  //       component={SignIn} 
  //       options={{title: 'Sign In'}}   
  //     />
  //     <AuthStack.Screen 
  //       name="CreateAccount" 
  //       component={CreateAccount} 
  //       options={{title: 'Create Account'}}
  //     />
  //   </AuthStack.Navigator> */}
  // </NavigationContainer>