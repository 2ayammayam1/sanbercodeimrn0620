import React, { Component, } from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import SkillItem from './components/SkillItem'
import skillData from './skillData.json' 

const skills = ["Library/Framework", "Bahasa Pemrograman", "Teknologi"];

export default class SkillScreen extends Component {
    
    render() {
        return (
            <View style={styles.container}> 
                <View style={styles.header}>
                    <Image style={styles.image} source={require('./images/sanberlogo.png')} />
                    <Text style={styles.logoText}>PORTOFOLIO</Text>
                </View>

                <View style={styles.profile}>
                    <MaterialCommunityIcons name="account-circle" size={30} color="#3EC6FF" />
                    <View style={styles.profileText}>
                        <Text style={{color: '#003366', fontSize: 12}}>Hai,</Text>
                        <Text style={{color: '#003366', fontSize: 16}}>Mukhlis Hanafi</Text>
                    </View>
                </View>

                <View>
                    <Text style={styles.titleText}>SKILL</Text>
                    <View style={styles.horizontalLine} />
                </View>
                <ScrollView contentContainerStyle={styles.category}>
                    { 
                        skills.map( (item, index) => {
                            return (
                                <TouchableOpacity style={styles.categoryItem} key={index}>
                                    <Text style={styles.categoryItemText}>{item}</Text>
                                </TouchableOpacity>
                            )
                        }

                        )
                    }
                </ScrollView>
                <FlatList
                    data={skillData.items}
                    renderItem={(skill)=><SkillItem skill={skill.item} />}
                    keyExtractor={(item)=>item.id.toString()}
                    ItemSeparatorComponent={()=><View style={{height:0.5}}/>}

                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20
    },
    header: {
        paddingTop: 60,
        alignItems: 'flex-end'
      }, 
    image: {
        width: 140,
        height: 23
      },
    logoText: {
        color: '#3EC6FF',
        fontSize: 12,
        textAlign: 'left'
    },
    profile: {
        flexDirection: 'row', 
        alignItems: 'center',
    },
    profileText: {
        marginLeft: 8,
    },
    titleText: {
        fontSize: 30,
        color: '#003366'
    },
    horizontalLine: {
        borderBottomColor: '#3EC6FF',
        borderBottomWidth: 4,
        marginVertical: 4
    },
    category: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 10
    },
    categoryItem: {
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        padding: 5,
        marginHorizontal: 10
    },
    categoryItemText: {
        fontSize: 12,
        color: '#003366',
        fontWeight: 'bold'
    }
  })