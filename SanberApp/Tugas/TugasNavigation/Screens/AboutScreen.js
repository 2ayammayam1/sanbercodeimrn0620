import React, {Component} from 'react';
import { StyleSheet, Image, Text, View} from 'react-native';

import { FontAwesome5 } from '@expo/vector-icons'; 
import { MaterialCommunityIcons } from '@expo/vector-icons'; 

export default class AboutScreen extends Component {
    render(){
    const contactList = [
        {
            name: 'M Arief Ismirianda',
            icon: 'facebook'
        },
        {
            name: '@mariefismirianda',
            icon: 'instagram'
        },
        {
            name: 'marief.ismirianda@gmail.com',
            icon: 'mail-bulk'
        }
        ]

    return (
      <View style={styles.container}>
        <View style={{alignItems: "center"}}>
          <Text style={styles.titleText}>Tentang Saya</Text>
          <Image 
            source={require('./images/profile.jpg')}
            style={styles.circularImage}
          />
          <Text style={styles.nameText}>Muhammad Arief Ismirianda</Text>
          <Text style={styles.roleText}>Game Developer</Text>
        </View>
        <View>
        <View style={styles.container2}>
          <View>
            <Text style={styles.subtitleText}>Portofolio</Text>
            <View style={styles.horizontalLine} />
          </View>
          <View style={styles.portoList}>
            <View style={{alignItems: 'center'}}>
            <MaterialCommunityIcons name="web" size={40} color="#3EC6FF" />
              <Text style={styles.itemText}>mariefismi02.github.io</Text>
            </View>
            <View style={{alignItems: 'center'}}>
              <FontAwesome5 name="github" size={40} color="#3EC6FF" />
              <Text style={styles.itemText}>@mariefismi02</Text>
            </View>
          </View>
        </View>
        </View>
        <View style={styles.container2}>
          <View>
            <Text style={styles.subtitleText}>Hubungi Saya</Text>
            <View style={styles.horizontalLine} />
          </View>
          <View>
            <View style={{justifyContent: 'center', flexDirection: 'row', paddingVertical: 4}}>
              <FontAwesome5 name="facebook" size={40} color="#3EC6FF" />
              <Text style={styles.itemText}>M Arief Ismirianda</Text>
            </View>
            <View style={{justifyContent: 'center', flexDirection: 'row', paddingVertical: 4}}>
              <FontAwesome5 name="instagram" size={40} color="#3EC6FF" />
              <Text style={styles.itemText}>@mariefismirianda</Text>
            </View>
          </View>
        </View>
      </View>
    );
    }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  titleText: {
    textAlign: 'center',
    color: '#003366',
    fontSize: 36,
    fontWeight: "bold",
    marginTop: 40
  },
  circularImage: {
    width: 200 , 
    height: 200, 
    borderRadius: 200/ 2,
    marginVertical: 20
  }, 
  nameText: {
    textAlign: 'center',
    color: '#003366',
    fontSize: 24,
    fontWeight: "bold",
    marginTop: 10
  },
  roleText: {
    textAlign: 'center',
    color: '#3EC6FF',
    fontSize: 16,
    fontWeight: "bold",
    marginVertical: 5
  },
  container2: {
    backgroundColor: '#EFEFEF',
    marginHorizontal: 8,
    marginTop: 8
  },
  subtitleText: {
    color: '#003366',
    fontSize: 18,
    marginLeft: 8,
    marginTop: 5
  },
  horizontalLine: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginVertical: 8
  },
  portoList: {
    flexDirection: 'row',
    marginVertical: 8,
    justifyContent: "space-around"
  },
  itemText: {
    color: '#003366',
    fontSize: 16,
    fontWeight: "bold",
    marginVertical: 8
  },
  constactList: {

  }
  
});
