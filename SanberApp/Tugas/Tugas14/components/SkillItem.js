import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'; 


export default class SkillItem extends Component {
    render() {
        let skill = this.props.skill;
        return (
            <TouchableOpacity style={styles.skillItem}>
            <MaterialCommunityIcons name={skill.iconName} size={72} color="#003366" />
                <View>
                    <Text style={styles.skillNameText}>{skill.skillName}</Text>
                    <Text style={styles.skillCategoryText}>{skill.categoryName}</Text>
                    <Text style={styles.skillScoreText}>{skill.percentageProgress}</Text>
                </View>
                <MaterialCommunityIcons name="chevron-right" size={72} color="#003366" />
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    skillItem: {
        backgroundColor: '#B4E9FF',
        flexDirection: 'row',
        marginVertical: 8,
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingTop: 10,
        paddingBottom: 8,
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    }, 
    skillNameText: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
    skillCategoryText: {
        fontSize: 16,
        color: '#3EC6FF',
        fontWeight: 'bold'
    },
    skillScoreText: {
        fontSize: 48,
        color: '#FFF',
        fontWeight: 'bold',
        alignSelf: 'flex-end'
    }
})