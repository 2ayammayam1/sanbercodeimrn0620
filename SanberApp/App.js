import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Navigation from "./Tugas/Quiz3/Index";
export default class App extends Component {
  render() {
    return (
      <Navigation />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})