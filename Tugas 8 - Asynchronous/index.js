// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
const start = (index, time) => {
    index > books.length - 1 ? '' : readBooks(time, books[index], (time) => {
        return start(index + 1, time) 
    })
}

// Tulis code untuk memanggil function readBooks di sini
start(0, 10000)

