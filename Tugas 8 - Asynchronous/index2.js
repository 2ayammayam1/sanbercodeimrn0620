var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
const start = (index, time) => {
    index > books.length - 1 ? '' : readBooksPromise(time, books[index])
    .then((time) => {
        return index+start(index + 1, time) ;
    }).catch((error) => {
        console.log(error);
    })
}

// Tulis code untuk memanggil function readBooks di sini
start(0, 10000)