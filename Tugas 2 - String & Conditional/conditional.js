var nama = "as";
var peran = "guard"

if(!nama){
    console.log("nama harus diisi!");
} else if(!peran){
    console.log(`halo ${nama}, peran harus diisi!`);
} else {
    if(peran === "Penyihir" || peran === "penyihir"){
        console.log(`Halo ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
    } else if(peran === "guard" || peran === "Guard"){
        console.log(`Halo ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
    } else if(peran === "werewolf" || peran === "Werewolf"){
        console.log(`Halo ${nama}, Kamu akan memakan mangsa setiap malam!`);
    } 
}