let range = (startNum, finishNum) => {
    var val = startNum;
    var arr = [];
    if(startNum == null || finishNum == null)
        return -1; 
    
    for(let i=0; i<Math.abs(finishNum-startNum)+1; i++){
        arr[i] = val;
        val = (startNum<=finishNum) ? val+1 : val-1;
    }

    return arr;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 