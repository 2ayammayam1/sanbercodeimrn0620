const dataHandling2 = (data) => {

    data.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    data.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(data)

    let date  = data[3].split("/");
    let month = "";
    switch(parseInt(date[1])){
        case 1: month = "Januari"; break;
        case 2: month = "Februari"; break;
        case 3: month = "Maret"; break;
        case 4: month = "April"; break;
        case 5: month = "Mei"; break;
        case 6: month = "Juni"; break;
        case 7: month = "Juli"; break;
        case 8: month = "Agustus"; break;
        case 9: month = "September"; break;
        case 10: month = "Oktober"; break;
        case 11: month = "November"; break;
        case 12: month = "Desember"; break;
    }
    console.log(month);

    let newDate = date.join("-");
    console.log(newDate);

    let newName = data[1].slice(0, 15);
    console.log(newName);

}

//contoh output
const input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 

dataHandling2(input)
