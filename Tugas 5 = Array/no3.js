let sum = (startNum, finishNum, step) => {
    var val = startNum;
    var arr = [];
    var result = 0;
    if(startNum == null)
        return 0; 
    if(finishNum == null)
        return startNum;
    if(step == null)
        step = 1;

    for(let i=0; i<Math.ceil((Math.abs(finishNum-startNum)+1)/step); i++){
        arr[i] = val;
        result += arr[i];
        val = (startNum<=finishNum) ? val+step : val-step;
    }

    return result;
}


console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 