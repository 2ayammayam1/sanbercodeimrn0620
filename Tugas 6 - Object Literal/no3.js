function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  objArrPenumpang = [];
  //your code here
  arrPenumpang.forEach((penumpang) => {
      
      //ongkos
      let started    = false;
      let cost       = 0;
      for(let i=0; i<rute.length; i++){
        if(!started){
            if(rute[i]===penumpang[1])
                started = true;
        } else {
            cost+=2000;
            if(penumpang[2]==rute[i])
                break;
        }
      }
      
      //obj
      const obj = {
          penumpang : penumpang[0],
          naikDari  : penumpang[1],
          tujuan    : penumpang[2],
          bayar     : cost
      }

      objArrPenumpang.push(obj);
  });

  return objArrPenumpang;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]