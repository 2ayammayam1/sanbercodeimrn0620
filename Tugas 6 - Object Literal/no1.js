const arrayToObject = (data) => {
    const objData = [];
    data.forEach((person) => {

        let now      = new Date()
        let thisYear = now.getFullYear() // 2020 (tahun sekarang)

        let myAge = (person[3]>thisYear || isNaN(person[3])) ? 
                    "Invalid birth year" : thisYear - person[3];

        const obj = {
            firstName: person[0],
            lastName : person[1],
            gender   : person[2],
            age      : myAge,
        }

        objData.push(obj);
    });

    objData.forEach((item, i) => {
        const fullName = item.firstName + " " + item.lastName;

        console.log(`${i+1}. ${fullName}: `, item);
    })
    
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""